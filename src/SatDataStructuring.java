import java.text.ParseException;

public class SatDataStructuring {

    private String satelliteId = "";
    private String severity = "";
    private String component ="";
    private String timestamp = "";

    public SatDataStructuring(String satelliteId, String severity, String component, String timestamp,
                              String redHigh, String redLow) throws ParseException {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = formatTime(timestamp);
    }
    public String formatTime (String timestamp) throws ParseException {

        String array[] = timestamp.split(" ");
        String dateFormated = array[0].substring(0,4) +"-" + array[0].substring(4,6) +
          "-" + array[0].substring(6,8) + "T" + array[1] + "Z";
        return dateFormated;

    }

    public String getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

}
