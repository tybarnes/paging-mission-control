import com.google.gson.Gson;

import java.io.*;
import java.text.ParseException;
import java.util.Scanner;

public class PagingMissionControl {

    String statusTelemetryData = "";
    Gson gson = new Gson();
    String json = "";

    PagingMissionControl(String statusTelemetryData) {
        this.statusTelemetryData = statusTelemetryData;
    }

    public void Ingest() {

        try {
            // open file to read
            Scanner scanner = new Scanner(new File(statusTelemetryData));

            // read until end of file (EOF)
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String array[] = line.split("\\|");
                double RawTemp = Double.valueOf(array[6]).doubleValue();
                int rh = Integer.valueOf(array[2]).intValue();
                int rl = Integer.valueOf(array [5]).intValue();

                switch (array[7]) {
                    case "TSTAT":
                        if (RawTemp > rh){
                            SatDataStructuring sds = new SatDataStructuring(array[1], "Red-High", array[7], array[0], array[2], array [5]);
                            json = gson.toJson(sds);
                            System.out.println(json);
                        }
                        break;

                    case "BATT":
                        if (RawTemp < rl){
                            SatDataStructuring sds = new SatDataStructuring(array[1], "Red-Low", array[7], array[0], array[2], array [5]);
                            json = gson.toJson(sds);
                            System.out.println(json);
                        }
                        break;
                }
            }
            // close the scanner
            scanner.close();

        } catch (FileNotFoundException ex) {
            System.out.println("An error occurred");
            ex.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
